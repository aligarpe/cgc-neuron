/*************************************************************

Copyright (c) 2020, Alicia Garrido Peña
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials provided
          with the distribution.
        * Neither the name of the author nor the names of his contributors
          may be used to endorse or promote products derived from this
          software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************/

#include <string>
#include <cstring>
#include <vector>
// #include "yaml-cpp/yaml.h"
#include "yaml.h"
using namespace std;

#define ERROR 0
#define OK 1

class InputParser
{
	public:	
		enum integrators{EULER,RUNGE,n_integrators};
		enum prim_types{String,Integer, Float, Double, IntegrationMeth, n_types};//<Data types for parsing function

	private:
		vector<string> arg_names;//<Arguments possible names
		vector<prim_types> arg_types;//Arguments corresponding types
		string yaml_name;//<Arguments possible names

	public:

		InputParser(vector< string > arg_names, vector< prim_types > arg_types)
		{
			this->arg_names = arg_names;
			this->arg_types = arg_types;
		}

		InputParser(char * yaml_name)
		{
			this->yaml_name = yaml_name;
		}

		int parse_input(int argc,char *argv[], void ** arguments);
		
		std::map<string, double> parse_file();

		
		void show_args();
};


