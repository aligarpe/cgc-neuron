/*************************************************************

Copyright (c) 2020, Alicia Garrido Peña
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials provided
          with the distribution.
        * Neither the name of the author nor the names of his contributors
          may be used to endorse or promote products derived from this
          software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************/

class EventsDetector
{
	double prev;
	double SPIKE_TH;
	double MIN_SPIKE_CHANGE;

	public:
		EventsDetector()
		{
			prev = 1;
			SPIKE_TH = -20.0;
			MIN_SPIKE_CHANGE = 0.001;
		}

		int detect_spikes(double t,double v, double dv)
		{
		  double dev;
		  int ret =0;

		  //0.001 less than that change in the derivative is not a spike
		  //NOTE: it might be necessary to adjust MIN_SPIKE_CHANGE for different spike shapes. 
		  if(prev >0 && dv <0 && (prev-dv)>MIN_SPIKE_CHANGE) //If it's a spike (from pos derivate to 0 derivate)
		  {
		    if(v >SPIKE_TH)
		      ret = 1;
		  }

		  prev=dv;
		  return ret;
		}

		void set_th(double th)
		{
			SPIKE_TH = th;
		}
		void min_change(double mn)
		{
			MIN_SPIKE_CHANGE = mn;
		}

};
