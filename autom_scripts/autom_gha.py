import sys
import os
import numpy as np 

os.system("echo `pwd`")

path = "gha"
step = "0.01"
os.system("mkdir -p ./data/"+path)

# for i in np.arange(1.1,0,-0.02): #default is 1.03
for i in np.arange(1.1,0,-0.1): #default is 1.03
	os.system("echo '###############################'")
	os.system("echo gha value %f"%i)
	os.system("./bin/CGCNeuron -file_name ./data/"+path+"/"+path+" -step "+step+" -n_spikes 51 -secs_dur 30 -i_ext 0.1 -ghva %f -log_param %f"%(i,i))
	os.system("sh ./plot/plot_last.sh "+path)

cmd = "python3 ~/Workspace/scripts/laser/superpos_from_model.py -p ./data/"+path+"/ -dt "+step+" -rp \""+path+"\" -ti \"CGC-Neuron GHVA"+"\" -wt 50 -xl \"20 90\""
print(cmd)
os.system(cmd + " -sa n -sh n -st n")
os.system(cmd + " -sa y -sh n")
# os.system("python3 ~/Workspace/scripts/laser/stats_plot_model.py -p ./data/"+path+"/")


# path = "gd2"
# step = "0.01"
# os.system("mkdir -p ./data/"+path)

# for i in np.arange(1.5,0.6,-0.02): #default is 1.2
# 	os.system("echo '###############################'")
# 	os.system("echo gd value %f"%i)
# 	os.system("./bin/CGCNeuron -file_name ./data/"+path+"/"+path+" -step "+step+" -n_spikes 51 -secs_dur 30 -i_ext 0.1 -gd %f -log_param %f"%(i,i))
# 	os.system("sh ./plot/plot_last.sh "+path)

# cmd = "python3 ~/Workspace/scripts/laser/superpos_from_model.py -p ./data/"+path+"/ -dt "+step+" -rp \""+path+"\" -ti \"CGC-Neuron "+path+"\" -wt 50"
# print(cmd)
# os.system(cmd + " -sa n -sh n -st n")
# os.system(cmd + " -sa y -sh n")
# # os.system("python3 ~/Workspace/scripts/laser/stats_plot_model.py -p ./data/"+path+"/")