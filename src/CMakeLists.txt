set (NEUN_INCLUDE_DIRS ../lib/neun/include ../lib/neun/concepts ../lib/neun/models ../lib/neun/integrators ../lib/neun/wrappers ../lib/neun/archetypes)
include_directories(${NEUN_INCLUDE_DIRS} ${YAMLCPP_INCLUDE_DIR} ../lib/utils)

set ( EXECUTABLE_OUTPUT_PATH  ${CMAKE_SOURCE_DIR}/bin)

add_executable(CGCNeuron CGC-Neuron.cpp input_parser.cpp)
target_link_libraries(CGCNeuron ${YAMLCPP_LIBRARY})

add_executable(CGCNeuron-Q10 CGC-Neuron-Q10.cpp input_parser.cpp)
target_link_libraries(CGCNeuron-Q10 ${YAMLCPP_LIBRARY})

add_executable(CGCNeuron-electrical CGC-Neuron-electrical.cpp input_parser.cpp)
target_link_libraries(CGCNeuron-electrical ${YAMLCPP_LIBRARY})

add_executable(CGCNeuron-depol CGC-Neuron_depol-repol.cpp input_parser.cpp)
target_link_libraries(CGCNeuron-depol ${YAMLCPP_LIBRARY})
