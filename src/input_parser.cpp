/*************************************************************

Copyright (c) 2020, Alicia Garrido Peña
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials provided
          with the distribution.
        * Neither the name of the author nor the names of his contributors
          may be used to endorse or promote products derived from this
          software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************/

#include "../lib/utils/input_parser.h"
#include <iostream>
int InputParser::parse_input(int argc,char *argv[], void ** arguments)
{

	int * aux_i;
	char ** aux_s;
	double * aux_d;
	float * aux_f;

	int num_args = arg_names.size();

	for(int i=1; i<argc; i+=2)
	{
		//Look for the argument
		for(int j=0; j<num_args; j++)
		{
			if(argv[i][0]!='-')
			{
				break;
			}
			if( strcmp(argv[i], arg_names[j].c_str()) == 0)
			{
				switch(arg_types[j])
				{
					case Integer:
						aux_i = (int *) arguments[j];
						*aux_i = atoi(argv[i+1]);
						break;
	
					case String:
						aux_s =(char **) arguments[j];
						*aux_s =argv[i+1];
						break;
	
	
					case Float:
						aux_f = (float *) arguments[j];
						*aux_f = atof(argv[i+1]);
						break;
					
					case Double:
						aux_d = (double *) arguments[j];
						*aux_d = atof(argv[i+1]);
						break;
					
					case IntegrationMeth:
						aux_i = (int *) arguments[j];
						if (strcmp(argv[i+1], "-e") == 0){
							*aux_i = 0;
						}
						else if(strcmp(argv[i+1], "-r") == 0)
							*aux_i = 1;

						break;

					default:
						cout << "Argument type not defined" << endl;
						return ERROR;

				}
				break;
			}
			else if(j == num_args-1)
			{
				cerr << "Incorrect argument key: " << argv[i]  << endl;
						return ERROR;
			}

		}
	}

	return OK;

}


std::map<string,double> InputParser::parse_file()
{
	// std:string yaml_file;
	std::map<string,double> config_values;
	std::cout << yaml_name << endl;
	YAML::Node config = YAML::LoadFile(this->yaml_name);


	for (YAML::const_iterator it=config.begin();it!=config.end();++it) {
	  // std::cout << it->first.as<std::string>() << " " << it->second.as<double>() << "\n";
	  double n = it->second.as<double>();
	  config_values.insert(std::pair<string,double>(it->first.as<std::string>(), it->second.as<double>()));
	}


	// for (int i=0; i < config_values.size(); i++)
	// 	std::cout << config_values[i] << endl;
	// std::map<string,double>::iterator it;
	// 	for (it=config_values.begin(); it!=config_values.end(); ++it)
	// 		std::cout << it->first << " => " << it->second << '\n';



	return config_values;
}


void InputParser::show_args()
{
	for (int i=0; i < arg_names.size(); i++)
		cout << arg_names[i] << " " << arg_types[i] << endl;
}
