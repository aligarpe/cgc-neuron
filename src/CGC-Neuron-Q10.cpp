#include <iostream>
#include <vector>
#include <string>
#include "../lib/neun/integrators/RungeKutta4.h"
#include "../lib/neun/integrators/Euler.h"
// #include "../lib/neun/models/VavoulisCGCModel.h"
#include "../lib/neun/models/VavoulisCGCModelQ10.h"
#include "../lib/neun/wrappers/IntegratedSystemWrapper.h"
#include "../lib/utils/input_parser.h"
#include "../lib/utils/events_detector.h"

// typedef Euler Integrator;
typedef RungeKutta4 Integrator;
typedef IntegratedSystemWrapper<VavoulisCGCModelQ10<double>, Integrator> Neuron;

using namespace std;

#define MAX_STRING 100000

int main(int argc, char * argv[]) {
  double max_time = 10.0;
  int n_spikes=10;
  double step = 0.001;
  double Cm = 1;
  double i_ext = 0.1;
  double gnat = 1.68;
  double gnap = 0.44;
  double ga = 18.82;
  double gd = 1.2;
  double glva = 0.01;
  double ghva = 1.03;
  double v0 = -65;
  double prev = 1.0;
  double diff_T=0;
  double general_Q10 = 3;

  char * file_name;
  char * yaml_name;
  char file_spikes[MAX_STRING];
  char file_log[MAX_STRING];
  char file_ext[MAX_STRING/2];
  char * log_param;
  const char * header;


  //////////////////////////////////////////////////////
  /////////////Parsing input 
  //////////////////////////////////////////////////////
  // string format = "Format: ./CGC-Neuron -file_name val -step val -n_spikes val -secs_dur val -i_ext val -Cm val -gnat val -gnap val -ga val -gd val -glva val -ghva val -log_param [Cm 0,gna 1,gk 2,gna-gk 3] -diff_T -general_Q10\n";//<Input format

  // vector<string> arg_names = {"-file_name","-step","-n_spikes","-secs_dur","-i_ext","-Cm","-gnat","-gnap","-ga","-gd","-glva","-ghva","-log_param", "-diff_T", "-general_Q10"};
  // vector<InputParser::prim_types> arg_types = {InputParser::String,InputParser::Double,InputParser::Integer,InputParser::Double,InputParser::Double,InputParser::Double,InputParser::Double,InputParser::Double,InputParser::Double,InputParser::Double,InputParser::Double,InputParser::Double,InputParser::String,InputParser::Double,InputParser::Double};

  string format = "Format: ./CGC-Neuron-Q10 -yaml_name val -file_name val -step val -n_spikes val -secs_dur val -i_ext val\n";//<Input format

  vector<string> arg_names = {"-file_name","-yaml_name","-step","-n_spikes","-secs_dur","-i_ext"};
  vector<InputParser::prim_types> arg_types = {InputParser::String,InputParser::String,InputParser::Double,InputParser::Integer,InputParser::Double,InputParser::Double};

  if(argc == 1){
    cout <<format<< endl;
    cout << "If any of those parameters is skiped, the default value will be assigned\n ./CGC-Neuron --help for more info" << endl;
    return -1;
  }
  else if(argc==2 && strcmp(argv[1],"--help")==0)
  {
    // InputParser::show_args();
    cout <<format<< endl;
    cout << "Input not valid" << endl;
    return -1;
  }
  else
  {
    InputParser input_parser(arg_names,arg_types);

    // void * arguments[] = {&file_name,&step,&n_spikes,&max_time,&i_ext,&Cm,
    //   &gnat,&gnap,&ga,&gd,&glva,&ghva,&log_param, &diff_T, &general_Q10};
    void * arguments[] = {&file_name,&yaml_name,&step,&n_spikes,&max_time,&i_ext};

    if(input_parser.parse_input(argc,argv,arguments)==ERROR)
    {
      cerr << "Error parsing input"<< endl;
      return -1;
    }
    if(!file_name)
    {
      cerr<< "No file name specified"<<endl;
      return -1;
    }
  }


  InputParser yaml_parser(yaml_name);

  //////////////////////////////////////////////////////
  /////////////Open files and set headers 
  //////////////////////////////////////////////////////
  //Add Iinj values
  // sprintf(file_ext,"%.3f_%.3f_%.3f_%.3f_%.3f_%.3f_%.3f_%.3f",
  //     i_ext,Cm,gnat,gnap,ga,gd,glva,ghva);
  sprintf(file_ext,"");

  //Join file name with parameters extension in spikes and basis file. 
  sprintf(file_spikes,"%s%s_spikes.asc",file_name,file_ext);
  sprintf(file_log,"%s%s_params.log",file_name,file_ext);
  sprintf(file_name,"%s%s.asc",file_name,file_ext);

  FILE * f = fopen(file_name,"w");
  FILE * f_spks = fopen(file_spikes,"w");
  FILE * f_log = fopen(file_log,"w");
  if(!f_log)
  {
    cerr << "FLOG NO\n"<< file_log << endl;
    return -1;

  }

  if(!f|!f_spks)
  {
    printf("Error opening files\n" );
    printf("%s\n",file_name );
    printf("%s\n",file_spikes );
    return -1;
  }

  cout << "Saving data in:" << endl;
  cout << file_name << "\n" << file_spikes << "\n" << file_log << endl;
  
  //Write File header 
  // header = "t V";
  header = "t V Inat Inap Ia Id Ilva Ihva";

  fprintf(f_spks, "%s\n",log_param );
  fprintf(f_spks, "%s\n",header );
  fprintf(f, "%s\n",header );

  //////////////////////////////////////////////////////
  /////////////Init neuron vals 
  //////////////////////////////////////////////////////

  Neuron::ConstructorArgs args;  // t, tau_p, tau_q, g_eca, g_ecs, n_parameters

  std::map<string,double> v_args = yaml_parser.parse_file();

  std::vector<std::string> param_names = Neuron::ParamNames();

  // for(int i=0; i < Neuron::n_parameters; i++)
  //   args.params[i] = general_Q10;

  for(int i=0; i < Neuron::n_parameters; i++)
  {
    // std::cout << v_args[param_names[i]] << " " << param_names[i] << endl;
    args.params[i] = v_args[param_names[i]];
  }

  // cout << "{";
  fprintf(f_log, "{");
  for(int i=0; i < Neuron::n_parameters; i++)
  {
    // cout << "\"" << param_names[i]  << "\"" << ":"<< args.params[i] << "," << endl;
    fprintf(f_log,"\"%s\":%.2f,\n",param_names[i].c_str(),args.params[i]);
  }
  fprintf(f_log,"\"general_Q10\":%.2f,\n",general_Q10);

  fseek(f_log, -2, SEEK_END);
  fprintf(f_log, "\n}\n");
  // cout << "}" << endl;
  fclose(f_log);

  Neuron cgc(args);

  cgc.set(Neuron::v, v0);
  cgc.set(Neuron::h, 1 / (1 + exp( (args.params[Neuron::vh_h] - v0) / args.params[Neuron::vs_h])));
  cgc.set(Neuron::r, 1 / (1 + exp( (args.params[Neuron::vh_r] - v0) / args.params[Neuron::vs_r])));
  cgc.set(Neuron::a, 1 / (1 + exp( (args.params[Neuron::vh_a] - v0) / args.params[Neuron::vs_a])));
  cgc.set(Neuron::b, 1 / (1 + exp( (args.params[Neuron::vh_b] - v0) / args.params[Neuron::vs_b])));
  cgc.set(Neuron::n, 1 / (1 + exp( (args.params[Neuron::vh_n] - v0) / args.params[Neuron::vs_n])));
  cgc.set(Neuron::e, 1 / (1 + exp( (args.params[Neuron::vh_e] - v0) / args.params[Neuron::vs_e])));
  cgc.set(Neuron::f, 1 / (1 + exp( (args.params[Neuron::vh_f] - v0) / args.params[Neuron::vs_f])));


  EventsDetector events;
  int spike=0, spike_count=0;
  max_time *= 1000; //s to ms
  // for (double time = 0.0; time < max_time; time += step) {
  double time = 0.0;

  // cout << cgc.get(Neuron::Q10_a) << " " << cgc.get(Neuron::Q10_Gnat) << endl;

  while(time < max_time && spike_count < n_spikes)
  {
    cgc.add_synaptic_input(i_ext);
    // double syn = n3t.get_synaptic_input();
    cgc.step(step);

    // fprintf(f, "%f %f\n",time,cgc.get(Neuron::v));
    fprintf(f, "%f %f %f %f %f %f %f %f\n",time,cgc.get(Neuron::v), cgc.get(Neuron::Inat), cgc.get(Neuron::Inap), cgc.get(Neuron::Ia), cgc.get(Neuron::Id), cgc.get(Neuron::Ilva), cgc.get(Neuron::Ihva));
    spike = events.detect_spikes(time,cgc.get(Neuron::v),cgc.get(Neuron::dv));

    //Ignore first and last spike
    if(spike and spike_count >0 and spike_count <n_spikes-1)
      fprintf(f_spks,"%f %f\n",time,cgc.get(Neuron::v));

    spike_count+= spike;
    time += step;
  }
  printf("Number of spikes %d\n",spike_count );

  fclose(f_spks);
  fclose(f);
}


