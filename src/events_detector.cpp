class EventsDectector
{
	double prev;

	EventsDectector()
	{
		prev = 1;
	}

	int detect_spikes(double t,Neuron neu)
	{

	  // bool fst_inrow = true;
	  // string buff ="";
	  double dev;
	  int ret =0;

	  dev = neu.get(Neuron::dv);

	  // cout << *prev << " " << dev << endl;
	  //0.001 less than that change in the derivative is not a spike
	  //NOTE: it might be necessary to adjust MIN_SPIKE_CHANGE for different spike shapes. 
	  if(prev >0 && dev <0 && (prev-dev)>MIN_SPIKE_CHANGE) //If it's a spike (from pos derivate to 0 derivate)
	  {
	    if(neu.get(Neuron::v) >SPIKE_TH)
	      // fprintf(f_spks,"%f %f\n",t,neu.get(Neuron::v));
	      ret = 1;
	  }

	  prev=dev;

	  return ret;

	  
	}


};
