# README #

Code use for simulations in CGC model using NEUN library (https://github.com/angellareo/NEUN). 

### Publications using this code ###
If you use this code please cite:

Garrido-Peña, Alicia, Sánchez-Martín, Pablo, Reyes-Sanchez, Manuel, Levi, Rafael, Rodriguez, Francisco B., Castilla, Javier, Tornero, Jesús, Varona, Pablo. Modulation of neuronal dynamics by sustained and activity-dependent continuous-wave near-infrared laser stimulation. Submitted.

Usefull analysis scripts for this data can be found at:

https://github.com/GNB-UAM/Garrido-Pena_Modulation-neural-dynamics-by-CW-NIR-stimulation

### How to run ###
Choose the specific script necessary. 
CGCNeuron takes parameters by terminal. Example:
	
	./bin/CGCNeuron -file_name ./data/Cm -step 0.01 -n_spikes 51 -secs_dur 30 -i_ext 0.1
	./bin/CGCNeuron -file_name ./data/Cm -step 0.01 -n_spikes 51 -secs_dur 30 -i_ext 0.1 -Cm 0.5 -log_param 0.5
	
CGCNeuron-Q10 uses yaml parser, so a yaml file will be necessary. Example:
	
	./CGCNeuron-Q10 -yaml_name data/config/Q10_default.yaml -file_name data/temp_0_q3_exclude_NN -n_spikes 10
 
check *src/input_parser.cpp*

### How to include NEUN ###
Neun is included as a subrepository. To load it is necessary to clone it apart.


### Contact ###

* Alicia Garrido Peña
* alicia.garrido@uam.es
* aliciagp96@gmail.com